# TODO : Create the lambda role with aws_iam_role

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}
# TODO : Create lambda function with aws_lambda_function

resource "aws_lambda_function" "test_lambda" {
	filename      = "empty_lambda_code.zip"
	handler       = "lambda_main_app.lambda_handler"
	runtime       = "python3.7"
	function_name = "job-processing-lambda"
	role          = aws_iam_role.iam_for_lambda.arn
}

# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Resource = "arn:aws:logs:*:*:log-group:/aws/lambda/*"
      }
    ]
  })
}

# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "lambda_logs_attachment" {
  policy_arn = aws_iam_policy.lambda_logging.arn
  role       = aws_iam_role.iam_for_lambda.name
}

# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "lambda_logs_AmazonS3FullAccess" {
  policy_arn = var.s3_full_access_policy
  role       = aws_iam_role.iam_for_lambda.name
}


# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_lambda_permission" "s3_lambda_permission" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.arn
  principal     = "s3.amazonaws.com"
  
  source_arn = aws_s3_bucket.s3-job-offer-bucket-tardy-trovin-turki.arn
}





