# TODO : Create a s3 bucket with aws_s3_bucket
resource "aws_s3_bucket" "s3-job-offer-bucket-tardy-trovin-turki" {
  bucket = "s3-job-offer-bucket-tardy-trovin-turki"

  tags = {
    Name        = "Practical2Bucket"
    Environment = "Dev"
  }
  force_destroy = true
}



# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object


resource "aws_s3_object" "object" {
  bucket = "s3-job-offer-bucket-tardy-trovin-turki"
  key    = "job_offers/raw/"
  source = "/dev/null"
}
# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "lambda_trigger" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-tardy-trovin-turki.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }
}
