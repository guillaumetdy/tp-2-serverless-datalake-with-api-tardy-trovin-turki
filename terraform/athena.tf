# TODO : Create athena database with aws_athena_database

resource "aws_athena_database" "esme" {
  name   = "esme"
  bucket = aws_s3_bucket.s3-job-offer-bucket-tardy-trovin-turki.id
  force_destroy = true
}


resource "aws_glue_catalog_table" "aws_glue_table_job_offers" {
  name          = "job_offers"
  database_name = aws_athena_database.esme.name
  table_type    = "EXTERNAL_TABLE"
  depends_on    = [aws_athena_database.esme]
  parameters = {
    classification = "parquet"
  }

  partition_keys {
    name = "city"
    type = "string"
  }
  partition_keys {
    name = "date"
    type = "string"
  }
  storage_descriptor {
    location      = "s3://${aws_s3_bucket.s3-job-offer-bucket-tardy-trovin-turki.id}/${var.processed_job_offers_key_name}"
    input_format  = "org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat"

    ser_de_info {
      name                  = "my-stream"
      serialization_library = "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"

      parameters = {
        "serialization.format" = 1
      }
    }

    columns {
      name = "id"
      type = "string"
    }
    columns {
      name = "company_name"
      type = "string"
    }
    columns {
      name = "title"
      type = "string"
    }
    columns {
      name = "url"
      type = "string"
    }
    columns {
      name = "contract_type"
      type = "string"
    }
    columns {
      name = "company_sector"
      type = "string"
    }
    columns {
      name = "user_experience"
      type = "string"
    }
    columns {
      name = "keyword"
      type = "string"
    }
    columns {
      name = "created_date"
      type = "string"
    }
    columns {
      name = "provider"
      type = "string"
    }
  }
}

resource "aws_athena_workgroup" "esme_workgroup" {
  name = "esme"

  configuration {
    enforce_workgroup_configuration    = true
    publish_cloudwatch_metrics_enabled = true

    result_configuration {
      output_location = "s3://${aws_s3_bucket.s3-job-offer-bucket-tardy-trovin-turki.bucket}/athena_results"
    }
  }
  force_destroy = true
}
